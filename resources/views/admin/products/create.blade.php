@extends('admin.layouts.master')

@section('page')
    Add Product
@endsection

@section('content')

<div class="row">
    <div class="col-lg-10 col-md-10">
            @include('admin.layouts.message')
        <div class="card">
            <div class="header">
                <h4 class="title">Add Product</h4>
            </div>
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="content">
                {!! Form::open(['url' => 'products', 'files'=>'true']) !!}
                    <div class="row">
                        <div class="col-md-12">
                            
                            @include('admin.products._fields')

                            <div class="">
                                {{ Form::submit('Add Product', ['class'=>'btn btn-primary']) }}
                            </div>
                        </div>

                    </div>
                    
                    <div class="clearfix"></div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection
